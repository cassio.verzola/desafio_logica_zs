package desafio_logica_zs;

import java.util.Scanner;

public class Desafio_12 {

	public static void main(String[] args) {

		/*
		 * Faça um programa que leia três valores (A, B, C) e mostre-os na ordem lida.
		 * Em seguida, mostre-os em ordem crescente e decrescente
		 */

		Scanner sc = new Scanner(System.in);

		int a, b, c;
		System.out.print("Digite os nmeros inteiros: ");
		a = sc.nextInt();
		b = sc.nextInt();
		c = sc.nextInt();
		System.out.println("Primeiro n lido: " + a + "\nSegundo n lido: " + b + "\nTerceiro n lido: " + c);

		System.out.println();
		System.out.println("====ORDEM CRESCENTE====");
		if (a < b) {
			 if (b < c) {
				System.out.println(a + " " + b + " " + c);
			} else if (a < c) {
				System.out.println(a + " " + c + " " + b);
			} else {
				System.out.println(c + " " + a + " " + b);
			}
		} else if (b < c) {
			if (a < c) {
				System.out.println(b + " " + a + " " + c);
			} else {
				System.out.println(b + " " + c + " " + a);
			}

		} else {
			System.out.println(c + " " + b + " " + a);
		}

		System.out.println();
		System.out.println("====ORDEM DECRESCENTE====");

		if (a > b) {
			if (b > c) {
				System.out.println(a + " " + b + " " + c);
			} else if (a > c) {
				System.out.println(a + " " + c + " " + b);
			} else {
				System.out.println(c + " " + a + " " + b);
			}
		} else if (b > c) {
			if (a > c) {
				System.out.println(b + " " + a + " " + c);
			} else {
				System.out.println(b + " " + c + " " + a);
			}

		} else {
			System.out.println(c + " " + b + " " + a);
		}

		sc.close();

	}

}
