package desafio_logica_zs;

public class Desafio_7 {

	public static void main(String[] args) {

		/*
		 * Criar um metodo simples que ir imprimir no console os numeros de 0  100.
		 * (montar logica utilizando for e while)
		 */

		for (int i = 0; i <= 100; i++) {
			System.out.println(i);

		}
		System.out.println();

		int i = 0;
		while (i <= 100) {
			System.out.println(i);
			i++;
		}
	}
}
