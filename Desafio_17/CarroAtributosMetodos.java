package desafio_logica_zs.Desafio_17;

public class CarroAtributosMetodos {

	/*
	 * Criar um m�todo que dever� pedir para o usu�rio informar no console os
	 * atributos de um carro (Marca, Cor, Quantidade de portas e se possui ou n�o
	 * airbag). Como desafio, esse m�todo dever� somente aceitar carros da marca
	 * BMW. O algoritmo dever� informar ao usu�rio que a marca do caso � invalida e
	 * pedir para que informe novamente at� que o dado esteja correto.
	 */

	public static void atributosCarro(String marca, String modelo, String cor, int portas) {
		if ("BMW".equals(marca)) {
			System.out.println("\nA modelo escolhido foi: BMW " + modelo);
			System.out.println("Todos os modelos da BMW possuem Airbag de s�rie!!");
			System.out.println("Cor escolhida: " + cor);
			System.out.println("Quantidade de portas:" + portas);

		} else {
			System.out.println("Marca inv�lida!!");
			System.out.println("A op��o deve ser por algum modelo da BMW. Por favor selecione algum modelo.");
		}

	}
}
