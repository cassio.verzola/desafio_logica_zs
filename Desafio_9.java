package desafio_logica_zs;

import java.util.Scanner;

public class Desafio_9 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		/*
		 * Criar um m�todo simples que ir� receber por par�metro um n�mero inteiro. A
		 * resposta desse m�todo deve informar se o n�mero � par ou �mpar.
		 */

		System.out.print("Digite um n�mero inteiro: ");
		int numeroDigitado = sc.nextInt();
		numeroParOuImpar(numeroDigitado);

		sc.close();
	}

	public static void numeroParOuImpar(Integer numero) {
		if (numero % 2 == 0) {
			System.out.println("O n�mero inteiro � par.");
		} else {
			System.out.println("O n�mero inteiro � impar.");
		}
	}
}