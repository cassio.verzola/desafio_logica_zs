package desafio_logica_zs.Desafio_17;

import java.util.Scanner;

public class Carro_BMW {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Informe a marca do carro: ");
		String marca = sc.next();

		System.out.print("Informe o modelo: ");
		String modelo = sc.next();

		System.out.print("Informe a cor: ");
		String cor = sc.next();

		System.out.print("Quantidade de portas: ");
		int portas = sc.nextInt();

		CarroAtributosMetodos.atributosCarro(marca, modelo, cor, portas);

		sc.close();

	}

}
