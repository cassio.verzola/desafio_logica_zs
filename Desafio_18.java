package desafio_logica_zs;

import java.util.Locale;
import java.util.Scanner;

public class Desafio_18 {

	public static void main(String[] args) {

		/*
		 * Faca um programa que apresente o menu de opcoes a seguir: Menu de opcoes: 1.
		 * Media aritmetica 2. Media ponderada 3. Sair Digite a opcao desejada: Na opcao
		 * 1: receber duas notas, calcular e mostrar a media aritmetica. Na op��o 2:
		 * receber tres notas e seus respectivos pesos, calcular e mostrar a media
		 * ponderada. Na opcao 3: sair do programa. Verifique a possibilidade de opcao
		 * invalida, mostrando uma mensagem.
		 */

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		System.out.println("\nMenu de opcoes:\n " + "\n1. media aritmetica.");
		System.out.println("2. media ponderada. \n" + "3. sair.");
		double nota1 = 0;
		double nota2 = 0;
		double nota3 = 0;

		System.out.print("\nDigite uma opcao: ");

		int opcao = sc.nextInt();

		switch (opcao) {
		case 1:
			System.out.println("\nMedia aritmetica \n" + "Digite as notas: \n");
			System.out.print("Nota 1: ");
			nota1 = sc.nextDouble();
			System.out.print("Nota 2: ");
			nota2 = sc.nextDouble();
			System.out.printf("\nA media aritmetica e: %.2f%n", (nota1 + nota2) / 2);
			break;

		case 2:
			System.out.println("\nMedia ponderada\n" + "Digite as notas: \n");
			System.out.print("Nota 1: ");
			nota1 = sc.nextDouble();
			System.out.print("Nota 2: ");
			nota2 = sc.nextDouble();
			System.out.print("Nota 3: ");
			nota3 = sc.nextDouble();
		System.out.printf("\nA media ponderada e: %.2f%n", (nota1 * 1 + nota2 * 2 + nota3 * 3) / 6);
			break;

		case 3:
			System.out.println("\nVoce saiu do programa.");
			break;

		default:
			System.out.println("\nOpcao invalida!!");
			break;
		}
		sc.close();

	}

}
