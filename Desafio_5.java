package desafio_logica_zs;

public class Desafio_5 {

	public static void main(String[] args) {

//		Faca um programa que mostre as tabuadas dos numeros de 1 a 10 usando lacos de repeticao.

		for (int i = 0; i <= 10; i++) {
			calcularTabuada(i);
			
		}

	}

	public static void calcularTabuada(Integer numero) {
		for (int i = 0; i <= 10; i++) {
			int x = numero * i;
			System.out.println(numero + " x " + i + " = " + x);
		}
		System.out.println("-------------");
	}
}
