package desafio_logica_zs;

import java.util.Scanner;

public class Desafio_4 {

	public static void main(String[] args) {

//		Faca um programa que receba um numero e usando lacos de repeticao calcule e mostre a tabuada desse numero.

		Scanner sc = new Scanner(System.in);

		System.out.print("A tabuada de qual numero voce deseja? Digite o numero: ");
		int N = sc.nextInt();
		System.out.print("Total de numeros multiplicados: ");
		int totalNumeros = sc.nextInt();

		for (int i = 0; i <= totalNumeros; i++) {
			int resultado = i * N;
			System.out.println(N + " x " + i + " = " + resultado);
		}

		sc.close();
	}

}
