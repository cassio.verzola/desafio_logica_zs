package desafio_logica_zs;

import java.util.Scanner;

public class Desafio_11 {

	/*
	 * Criar um m�todo simples que ir� receber por par�metro uma data. O m�todo
	 * dever� imprimir no console qual dia da semana essa data pertence (Ex:
	 * segunda, ter�a, quarta, etc.)
	 */

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Digite um dia: ");
		int dia = sc.nextInt();
		diaDaSemana(dia);

		sc.close();
	}

	public static void diaDaSemana(int dia) {

		switch (dia) {
		case 1:
			System.out.println("Domingo");
			break;
		case 2:
			System.out.println("Segunda-feira");
			break;
		case 3:
			System.out.println("Ter�a-feira");
			break;
		case 4:
			System.out.println("Quarta-feira");
			break;
		case 5:
			System.out.println("Quinta-feira");
			break;
		case 6:
			System.out.println("Sexta-feira");
			break;
		case 7:
			System.out.println("S�bado");
			break;
		default:
			System.out.println("Dia inv�lido!!");
			break;

		}
	}
}