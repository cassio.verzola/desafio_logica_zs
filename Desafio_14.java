package desafio_logica_zs;

import java.util.Locale;
import java.util.Scanner;

public class Desafio_14 {

	public static void main(String[] args) {

		/*
		 * Faca um programa que receba a idade e o peso de sete pessoas. Calcule e
		 * mostre: � A quantidade de pessoas com mais de 90 quilos; � A media das idades
		 * das sete pessoas
		 */

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		int idade = 0;
		double peso = 0.0;
		double soma = 0.0;
		int acimaPeso = 0;
		double mediaIdade = 0.0;

		for (int i = 0; i < 4; i++) {
			System.out.print("Digite a idade: ");
			idade = sc.nextInt();
			System.out.print("Digite o peso: ");
			peso = sc.nextDouble();

			if (peso > 90) {
				acimaPeso++;
			}
			soma = soma + idade;
			mediaIdade = soma / 4;
		}
		System.out.println("A quantidade de pessoas acima de 90 kg � de: " + acimaPeso + " pessoa(s).");
		System.out.printf("A m�dia das idades � de: %.1f anos.", mediaIdade);

		sc.close();

	}

}
