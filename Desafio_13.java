package desafio_logica_zs;

import java.util.Scanner;

public class Desafio_13 {

	public static void main(String[] args) {

		/*
		 * Fa�a um programa que receba a idade de 15 pessoas e que calcule e mostre: a)
		 * A quantidade de pessoas em cada faixa et�ria; b) A percentagem de pessoas na
		 * primeira e na �ltima faixa et�ria, com rela��o ao total de pessoas: � At� 15
		 * anos � De 16 a 30 anos � De 31 a 45 anos � De 46 a 60 anos � Acima de 61 anos
		 */

		Scanner sc = new Scanner(System.in);

		int idadeAte_15 = 0;
		int idadeEntre16_30 = 0;
		int idadeEntre31_45 = 0;
		int idadeEntre46_60 = 0;
		int idadeAcima_61 = 0;
		
		System.out.println("Digite as idades: ");
		for (int i = 0; i < 15; i++) {
			int idade = sc.nextInt();
			
			if (idade <= 15) {
				idadeAte_15++;
			} else if (idade >= 16 && idade <= 30) {
				idadeEntre16_30++;
			} else if (idade >= 31 && idade <= 45) {
				idadeEntre31_45++;
			} else if (idade >= 46 && idade <= 61) {
				idadeEntre46_60++;
			} else {
				idadeAcima_61++;
			}
		}
		System.out.println("--------------------------------------------------------");
		System.out.println("A quantidade de pessoas at� 15 anos � de: " + idadeAte_15 + " pessoas.");
		System.out.println("A quantidade de pessoas de 16 a 30 anos � de: " + idadeEntre16_30 + " pessoas. ");
		System.out.println("A quantidade de pessoas de 31 a 45 anos � de: " + idadeEntre31_45 + " pessoas. ");
		System.out.println("A quantidade de pessoas de 46 a 60 anos � de: " + idadeEntre46_60 + " pessoas. ");
		System.out.println("A quantidade de pessoas acima de 60 anos � de: " + idadeAcima_61 + " pessoas. ");
		System.out.printf("A percentagem de pessoas at� 15 anos � de: %.2f %%%n", (double)(idadeAte_15 * 100) / 15);
		System.out.printf("A quantidade de pessoas acima de 60 anos � de: %.2f %%%n", (double)(idadeAcima_61 * 100) / 15);

		
		sc.close();
	}
}
