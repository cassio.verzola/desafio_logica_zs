package desafio_logica_zs;

public class Desafio_8 {

	public static void main(String[] args) {

		/*
		 * Criar um metodo simples que ir imprimir no console os numeros de 100  0.
		 * (montar logica utilizando for e while)
		 */

		for (int i = 100; i >= 0; i--) {
			System.out.println(i);

		}
		System.out.println();

		int i = 100;
		while (i >= 0) {
			System.out.println(i);
			i--;

		}

	}
}
