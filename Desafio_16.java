package desafio_logica_zs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Desafio_16 {

	/*A prefeitura de uma cidade fez uma pesquisa entre seus habitantes, coletando dados sobre o salário e o número de
	filhos. A prefeitura deseja saber:
	• A média do salário da população;
    • A média do número de filhos;
    • O maior salário;
    • A percentagem de pessoas com salários até R$ 1500,00.*/

	private ArrayList<Double> quantidadesFilhos = new ArrayList<>();
	private ArrayList<Double> valorSalarios = new ArrayList<>();

	public static void main(String[] args) {

		Desafio_16 desafio_16 = new Desafio_16();

		System.out.print("Informe a população de sua cidade: ");

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		int N = sc.nextInt();
		double salario = 0.0;
		double filhos = 0;

		for (int i = 0; i < N; i++) {
			System.out.print("Valor do salario: ");
			salario = sc.nextDouble();
			System.out.print("Numero de filhos: ");
			filhos = sc.nextInt();

			desafio_16.gravaNumeroDeFilhos(filhos);
			desafio_16.gravarValorSalarios(salario);


		}

		System.out.printf("A média de filhos é de: %.2f filhos.%n", desafio_16.calculoMediaFilhos());
		System.out.printf("A média salarial é de: R$ %.2f%n", desafio_16.calculoMediaSalarios());
		System.out.printf("O maior salário é: R$ %.2f%n", desafio_16.calculoDoMaiorSalario());
		System.out.printf("A percentagem de pessoas que ganham até R$ 1500,00 é de: %.2f %%", desafio_16.percentagemPessoas());
		sc.close();
	}

	public void gravaNumeroDeFilhos(Double filhos) {
		quantidadesFilhos.add(filhos);
	}

	public double calculoMediaFilhos() {
		int populacao = quantidadesFilhos.size();
		double totalFilhos = 0;
		double mediaFilhos = 0;
		for (int i = 0; i < populacao; i++) {
			totalFilhos += quantidadesFilhos.get(i);
		}
		mediaFilhos = totalFilhos / populacao;
		return mediaFilhos;

	}

	public void gravarValorSalarios(Double salario) {
		valorSalarios.add(salario);
	}

	public double calculoMediaSalarios() {
		int remuneracao = valorSalarios.size();
		double totalSalario = 0;
		double mediaSalario = 0;
		for (int i = 0; i < remuneracao; i++) {
			totalSalario += valorSalarios.get(i);
		}
		mediaSalario = totalSalario / remuneracao;
		return mediaSalario;
	}

	public double calculoDoMaiorSalario() {
		int remuneracao = valorSalarios.size();
		double maiorSalario = 0;
		for (int i = 0; i < remuneracao; i++) {
			if (maiorSalario < valorSalarios.get(i)) {
				maiorSalario = valorSalarios.get(i);
			}
		}
		return maiorSalario;
	}

	public double percentagemPessoas() {
		double remuneracao = valorSalarios.size();
		double faixaSalarial = 0;
		double totalPessoas = 0;
		for (int i = 0; i < remuneracao; i++) {
			faixaSalarial = valorSalarios.get(i);
			if (faixaSalarial <= 1500.0) {
				totalPessoas++;
			}
		}
		return (totalPessoas * 100) / valorSalarios.size();
	}
}



