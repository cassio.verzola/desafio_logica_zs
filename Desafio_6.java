package desafio_logica_zs;

import java.util.Scanner;

public class Desafio_6 {

	public static void main(String[] args) {

		/*
		 * Faca um programa que receba a idade de dez pessoas e que calcule e mostre a
		 * quantidade de pessoas com idade maior ou igual a 18 anos.
		 */

		Scanner sc = new Scanner(System.in);

		System.out.println("Informe as idades: ");
		int idade;
		int totalMaiorIdade = 0;
		for (int i = 0; i < 10; i++) {
			idade = sc.nextInt();

			if (idade >= 18) {
				totalMaiorIdade++;
			}
		}
		System.out.println("Quantidade de pessoas com 18 anos ou mais  de: " + totalMaiorIdade + " pessoas.");
		sc.close();
	}
}
