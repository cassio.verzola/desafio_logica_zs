# desafio_logica_ZS

Exercícios resolvidos da prova de lógica que compreende como tarefa do estágio.

1 – Qual o valor de “a” para o código apresentado abaixo? Marque a alternativa correta.

a = 10
i = 0

while (i<10)
       while (i < 10)
                a = a + 1
                i = i + 1
       end while.
end while.

a) 10
b) 20
c) 30
d) 40
e) 50

Resposta: alternativa "b".


2 – Qual o valor de “a” para o código apresentado abaixo? Marque a alternativa correta.

a = 10
i = 0

for (i = 0, i < 10, i++)
     while (x < 5)
              x = x + 1
              a = a + 1
     end while
end for

a) 10
b) 20
c) 30
d) 40
e) 50

Resposta: valor encontrado = 15 (nenhuma alternativa correspondente)


3 – Marque a alternativa correspondente ao resultado retornado pela função CALCULA.

x = 5

function calcula (x)
         if x > 1
                 a = x – 1
                 return x * calcula (a)
       else
                 return 1
       end if
end calcula

imprimir calcula(x)

a) 100
b) 110
c) 120
d) 130
e) 140

Resposta: alternativa "c".


